(define (domain zombie-escape)
	(:requirements :typing :fluents)
	(:types human location)
	(:predicates
		; where is the given human
		(at ?h - human ?l - location)
		; where is the lantern
		(lantern_at ?l - location)
	)
	(:functions
		; how long it takes for the given human
		; to pass the bridge
		(crossing-time ?h - human)
		; how much time already passed
		(time-passed)
	)
	; one man crosses the bridge
	(:action cross-one-man
		; TODO: fill the action
	)
	; two men cross the bridge
	(:action cross-two-men
		; TODO: fill the action
	)
)