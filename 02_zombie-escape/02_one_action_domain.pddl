(define (domain zombie-escape)
	(:requirements :typing :fluents)
	(:types human location)
	(:predicates
		; where is the given human
		(at ?h - human ?l - location)
		; where is the lantern
		(lantern_at ?l - location)
	)
	(:functions
		; how long it takes for the given human
		; to pass the bridge
		(crossing-time ?h - human)
		; how much time already passed
		(time-passed)
	)
	; cross the bridge!
	(:action cross
		; TODO: fill the action
	)
)