(define (domain cannibals)
	(:requirements :typing :fluents)
	(:types kind location)
	(:predicates
		; where is the boat
		(boat_at ?l - location)
	)
	(:functions
		; how many people of a given kind is at the given location
	    (number_of ?k - kind ?l - location)
	)
	; TODO: actions
)